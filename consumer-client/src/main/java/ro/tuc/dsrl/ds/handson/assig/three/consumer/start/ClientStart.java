package ro.tuc.dsrl.ds.handson.assig.three.consumer.start;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import ro.tuc.dsrl.ds.handson.assig.three.consumer.connection.QueueServerConnection;
import ro.tuc.dsrl.ds.handson.assig.three.consumer.service.MailService;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems,
 *          http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description: Starting point for the Consumer Client application. This
 *               application will run in an infinite loop and retrieve messages
 *               from the queue server and send e-mails with them as they come.
 */
public class ClientStart {

	private ClientStart() {
	}

	public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection("localhost", 8888);

		MailService mailService = new MailService("r_bogdan94@yahoo.com", "rbo00094");
		String message;
		while (true) {
			try {
				message = queue.readMessage();
				System.out.println("Sending mail " + message);
				mailService.sendMail("r_bogdan94@yahoo.com", "Title", message);
				writeInFile("cacat.txt", message);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void writeInFile(String path, String message) {

		File log = new File(path);

		try {
			if (!log.exists()) {
				System.out.println("We had to make a new file.");
				log.createNewFile();
			}

			FileWriter fileWriter = new FileWriter(log, true);

			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write("******* " + message + "******* " + "\n");
			bufferedWriter.close();

			System.out.println("Done");
		} catch (IOException e) {
			System.out.println("COULD NOT LOG!!");
		}
	}

}
